FROM node:17-bullseye

RUN mkdir /foundry /foundry_data

COPY foundryvtt-0.8.9.zip /foundry

RUN cd /foundry && \
    unzip foundryvtt-0.8.9.zip

WORKDIR /foundry

CMD ["node", "resources/app/main.js", "--dataPath=/foundry_data"]